# Docker Drupal 8 environment for development

## How to use

* Clone this repo and cd to it;
* cd to project and run `composer install`
* cd to docker and copu `docker-compose.yml.dist` to `docker-compose.yml`
* make sure everything is oke in `docker-compose.yml`
* run `docker-compose up -d`
* wait fot build process
* login to container
* cd to `/var/www/project/web`
* run `../scripts/install-symlinks.sh`
* run `../scripts/project-install.sh`


# How to access docker container using its IP?

NOTE: Assuming you are on mac and using parallels.

* First find out your docker-machine IP by executing `docker-machine ip {YOUR_MACHINE_NAME}` or use the following command `docker inspect --format '{{ .NetworkSettings.IPAddress }}' ${CID}`
* When you have your machine IP execute this from your host (From your mac, not container) `sudo route -n add 172.17.0.0/16 {YOUR_MACHINE_IP}`;
* Now you can access all docker containers using their IP;
* To get specific docker container ip execute this `docker inspect {YOUR_CONTAINER_ID}`;
* You will get JSON output and look for IPAddress in it, thats your container`s IP;