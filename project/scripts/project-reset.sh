#!/bin/bash

# Load global configurations.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/config

cd $DOCROOT

chmod +w web/sites/default

if [ -f web/sites/default/settings.php ]; then
  chmod 777 web/sites/default/settings.php
  rm web/sites/default/settings.php
fi

if [ -f web/sites/default/settings.local.php ]; then
  chmod 777 web/sites/default/settings.local.php
  rm web/sites/default/settings.local.php
fi

if [ -f web/sites/default/services.yml ]; then
  chmod 777 web/sites/default/services.yml
  rm web/sites/default/services.yml
fi

chmod -w web/sites/default
