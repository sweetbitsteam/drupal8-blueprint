#!/bin/bash

# Load global configurations.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/config

# Constants
REFERENCE=$1
REF_CHAR=${REFERENCE:0:1}
ENVIRONMENT=$2


# Check out master branch
git clone $REPOSITORY release/$REFERENCE

cd release/$REFERENCE/

if [ $REF_CHAR = 'v' ]; then
  # Version tag given.
  echo "Checking out tag $REFERENCE"
  git checkout tags/$REFERENCE
else
  # Branch given.
  echo "Checking out branch $REFERENCE"
  git checkout $REFERENCE
fi

if [ $ENVIRONMENT = 'prod' ]; then
  composer install --no-dev
else
  composer install
fi

# Sync files directories
rsync -av --progress $DOCROOT/current/sites/default/files/ $DOCROOT/release/$REFERENCE/docroot/sites/default/files/
