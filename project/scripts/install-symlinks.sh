#!/bin/bash

rm -rf /var/www/html
ln -s /var/www/project/web/ /var/www/html

ln -s /var/www/project/vendor/bin/drush /usr/bin/drush

ln -s /var/www/project/vendor/bin/drupal /usr/bin/drupal
