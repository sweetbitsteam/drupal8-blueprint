#!/bin/bash

# Load global configurations.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/config


cd $DOCROOT

if [ -d "/var/www/project/web/sites/default" ]; then
  echo "exists"
else
  mkdir "/var/www/project/web/sites/default"
  mkdir "/var/www/project/web/sites/default/files"
  chmod -R 777 /var/www/project/web/sites/default/files
fi

chmod +w web/sites/default

chmod 777 web/sites/default/settings.php
cp environments/settings.php web/sites/default/
chmod 755 web/sites/default/settings.php

chmod 777 web/sites/default/services.yml
cp environments/services.yml web/sites/default/
chmod 755 web/sites/default/services.yml

if [ "$1" = 'prod' ]; then
  cp environments/settings.production.php web/sites/default/settings.local.php
elif [ "$1" = 'staging' ]; then
  cp environments/settings.staging.php web/sites/default/settings.local.php
else
  cp environments/settings.develop.php web/sites/default/settings.local.php
fi

chmod 755 web/sites/default/settings.local.php

chmod -R 777 /var/www/project/web/sites/default/files

cd web/

/usr/bin/env PHP_OPTIONS="-d sendmail_path=`which true`" \
$DOCROOT/vendor/bin/drush site-install config_installer -y --config-dir=../config/sync \
--account-name=$ADMIN_USER --account-pass=$ADMIN_PASS --account-mail=$ADMIN_EMAIL

chmod -w /var/www/project/web/sites/default/
chmod -R 777 /var/www/project/web/sites/default/files


# $DOCROOT/vendor/bin/drush user-password --password="$ADMIN_PASS" "$ADMIN_USER"
