#!/bin/bash

# Load global configurations.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/config

# Constants
REFERENCE=$1

cd $DOCROOT/current
# Dump database
DATE=$(date '+%Y%m%d-%H%I%S')
REVISION=$(git rev-parse --short HEAD)
drush sql-dump --gzip --skip-tables-key=common --result-file=$DOCROOT/database/$DATE-$REVISION.sql

drush drush vset maintenance_mode 1

cd $DOCROOT
rm current
ln -s release/$REFERENCE current
cd current
drush updatedb -y
drush fra -y
drush drush vset maintenance_mode 0
echo "Upgrade complete."
